# Javascript Versions  

## Introducció  
Javascript és un llenguatge de programació per al desenvolupament d'aplicacions web inicialment
estava dedicat al costat del client.    

## ES5
Avui en dia encara es troba suportat per navegadors moderns.

Desde el seu inici al 1997 fins al Desembre de 2009 Javascript va anar fent petites modificiacions a
les seves funcionalitats i sintaxis.  

### Context històric
Empreses com Yahoo, Microsoft i Google no estaven d'acord amb les funcuionalitats de la versió anterior de JS.  
Aquest van formar un subcomite per treure una versió millorada de EcmaScript 3 anomenada EcmaScript
3.1.   
Després d'algunes

### Novetats
Entre les més resenyables si troben:   
- **use strict;**: No et permet usar variables no declarades amb `var`. Ajuda a escriure codi més net.  
- **String.trim()**: Remoure espais al inici i final d'un String.
- **Arrays**: Va incloure moltes funcions per a tractar arrays. Com per exemple
  `forEach()`,`map()`,`indexOf()`.
- **Funcionalitats JSON**: Millor lectura i ús dels arxius en format JSON.
- **Date.now()**: Un gran afegit que permetia declara la data actual.

A més a més, de cara als objectes permetia declara uns tipus de funcions get i set, metodes propis
d'objectes, accedir a strings com si fossin arrays `[]`.

## ES6/ES2015


## ES6


# Comparar Cadenes

## Equalitat
Usan el comparador `==` o `===`.
Checkeja si els dos poerants son iguals.  
`"string" == "string"` -> true  
No checkeja el objecte ja que: `new String("a") == new String("a")` -> false.


## String.localCompare();
Compara dos strings al 'current locale' del browser.
Retorn -1, 0, 1 depenent de la posició.

## indexOf() i length()
Combinant el indexOf i la llargada de la caden es pot arribar a veure si les cadenes son iguals.

## Object.is()
Ja que els strings son objectes es pot determinar si dos objectes(strings en aquests cas) son iguals
amb el metode Object.is().

## valueOf()
`new String("a").valueOf() == new String("a").valueOf()`
Compara el valor de l'objecte string.