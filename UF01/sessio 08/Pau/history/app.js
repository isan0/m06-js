function goBack() {
	history.back();
}

function goForward() {
	history.forward();
}

function goRefresh() {
	history.go(history.length - 1);
}

const back_button = document.querySelector('.back');
const forward_button =  document.querySelector('.forward');
const refresh_button = document.querySelector('.refresh');

back_button.addEventListener('click', goBack);
forward_button.addEventListener('click', goForward);
refresh_button.addEventListener('click', goRefresh);
