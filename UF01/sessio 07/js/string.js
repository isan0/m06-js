
/******************************************************************
1. Fes un programa que donat un número de DNI, calculi si la lletra és correcta. 
******************************************************************/

/*
function validarDNI(dni) {
	//Tingues present que la lletra del DNI es calcula en dos passos:
	//Calcular el mòdul del número del DNI respecte 23
	//El resultat de l’operació anterior correspon a la posició del següent string de lletres que s’ha d’agafar:
	//lletres =”TRWAGMYFPDXBNJZSQVHLCKE”;
		var lletres = 'TRWAGMYFPDXBNJZSQVHLCKE';
		var num_dni=dni.substr(0,dni.length-1);
		var lletra_dni=dni.charAt(dni.length-1).toUpperCase();
		
		alert(num_dni % 23)
		if (lletres.charAt(num_dni % 23)==lletra_dni) return true;
			return false;
		}

console.log(validarDNI('87654321J')); //false
console.log(validarDNI('87654321X')); //true	
*/

/******************************************************************
2. Fes un programa que determini si un password és segur. 
******************************************************************/

/* 
Volem saber si una contrasenya és segura. Per tal que sigui segura ha de complir les següents regles:
Ha de contenir almenys una vocal.
No ha de tenir tres vocals consecutives o tres consonants consecutives.
No ha de tenir dues ocurrències consecutives de la mateixa lletra, excepte per ‘ee’ o ‘oo’.
La longitud mínima és de 6 caràcters i la màxima de 10.
Els espais en blanc no estan permesos.
*/

/*
var password = prompt("Escriu el password");
    var lowerCase = new RegExp("^.*([a-z]+).*$");
    var upperCase = new RegExp("^.*([A-Z]+).*$");
    var numbers = new RegExp("^.*([0-9]+).*$");
    var specialChars = new RegExp("^.*([.-_*!]+).*$");
    console.log(password.length);
    if (password.length >= 8) {
        if (lowerCase.test(password)) {
            if (upperCase.test(password)) {
                if (numbers.test(password)) {
                    if (specialChars.test(password)) {
                        window.alert("La contrasenya és segura!");
                    } else {
                        window.alert("Password no conté caràcters especials!");
                    }
                } else {
                    window.alert("Password no conté nombres!");
                }
            } else {
                window.alert("Password no conté majúscules!");
            }
        } else {
            window.alert("Password no conté minúscules!");
        }
    } else {
        window.alert("Password massa curta, al menys 8 caràcters!");
    }
*/


/******************************************************************
3. Fes un programa per validar el mail sigui correcte.
******************************************************************/

/*
var email = prompt("Escriu el teu email");
    var mailRegExp = new RegExp("^([a-zA-Z0-9._-]+@"+
    "[a-z]+[.].[a-z]{1,3})$");
    if (mailRegExp.test(email)) {
        window.alert("Email vàlid");
    } else {
        window.alert("Email invàlid");
    }

*/

/******************************************************************
4. Escriviu un programa per realitzar la compressió bàsica de cadenes utilitzant el recompte de caràcters repetits, per exemple, "aabcccccaaa" es convertiria en "a2b1c5a3", si no hi ha repetitcions, només cal que imprimiu l'original
******************************************************************/

 /*   var word = "aabcccccaaa";
    var result = "";
    var counter = 1;

    for(var i = 0; i <= word.length; i++) {
        if(word.substr(i, 1) === word.substr(i+1, 1)) {
            counter++;
        }
        else {
            result = result + word.substr(i, 1) + counter;
            counter = 1;
        }
    }

    console.log(result);

    if(result.length < word.length)
        console.log(result)
    else
        console.log(word);



























