## Parte 3: Crea una aplicación CRUD con Vue.js 2
En las siguientes lecciones aprenderemos a crear una pequeña aplicación CRUD y a manejar arrays de registros de forma dinámica con Vue.

12. [Introducción](https://styde.net/crea-tu-primera-aplicacion-con-vue-js-2/) - 4:40
13. [Listado](https://styde.net/listado-dinamico-con-vue-js-2/) - 7:17
14. [Creación](https://styde.net/creacion-de-registros-a-traves-de-formularios-con-vue-js-2/) - 1:31
15. [Actualización](https://styde.net/actualizacion-de-registros-con-vue-js-2/) - 5:13
16. [Eliminación](https://styde.net/eliminar-registros-de-un-listado-con-vue-js-2/) - 2:46