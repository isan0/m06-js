const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name:'',
      confirmedNamed:'',
    };
  },

  methods: {
    submitForm(){
      alert('submitted');
    },
    confirmedInput(){
      this.confirmedNamed = this.name;
    },
    setname(event, lastname){
      //aquí tenim tota la informació del element del element del html.
      this.name= event.target.value + ' ' + lastname  ;
    },
    add(num){
      this.counter= this.counter + num ;
    },
    reduce(){
      this.counter= this.counter - 1 ;
    }
  }
});
app.mount('#events');
