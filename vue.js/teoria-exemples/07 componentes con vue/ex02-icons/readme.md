## Parte 4: Creación y uso de componentes en Vue 2
En esta parte del curso aprenderás todo sobre la creación, uso y comunicación entre componentes.

17. [Creación del primer componente](https://styde.net/creacion-y-uso-de-tu-primer-componente-con-vue-js-2/) - 4:23
18. [Representar un registro como un componente](https://styde.net/representacion-de-un-registro-como-un-componente-en-vue-js-2/) - 11:05
19. [Separación de datos entre el modelo y la vista](https://styde.net/separacion-de-datos-entre-el-modelo-y-la-vista-en-vue-js-2/) - 9:25
20. [Comunicación entre componentes con eventos personalizados](https://styde.net/uso-de-eventos-personalizados-para-comunicar-componentes-en-vue-2/) - 4:14
21. [Comunicación entre componentes con "Event Bus"](https://styde.net/uso-del-event-bus-para-comunicar-componentes-en-vue-js-2/) - 5:23