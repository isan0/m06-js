/*
1. Afegirem una imatge de capçalera segons tipus usuari. En cas de que estigui 
seleccionat client és mostrarà una imatge. En cas de que sigui venedor en 
serà una altra. 

2. El color de la seccio/capa serà diferent segons el tipus de client. 

3. Crear estils diferents pel Client i pel venedor. Canvia l'estil dels CSS 
segons el tipus d'usuari. Per exemple color de font blau pel client  i color 
de font negre pel venedor.

4. Feel free d'afegir en el projecte qualsevol  cosa que t'hagis trobat pel 
camí i tinguis ganes de provar.  
*/
const vm = new Vue({
  el: "#myApp",
  data: {
    // generic data (not used for form validation) 
    vue: {
      // will toggle the vendor or client fields
      vendor: null,
      // used for form validation result display
      resultStr: '',
      img: '',
      textarea: '',
    },
    // data used for form validation (kept separate for better validation)
    valData: {
      name: '',
      surname: '',
      mail: '',
    },
  },
  computed: {
    // handles vendor or client class on myApp div
    handleClass() {
      if (this.vue.vendor != null) {
        return this.vue.vendor ? 'vendor': 'client';
      }
      return;
    },
    // handles the src value of the img depending if client or vendor is selected
    handleImg() {
      if (this.vue.vendor != null) {
        return this.vue.vendor ? 'img/vendor.jpg': 'img/client.jpg';
      }
      return;
    },
    // handles error message deppending if there is no text, too little or too much text
    handleText() {
      const val = this.vue.textarea.trim();
      if (val == '') {
        return 'Mandatory field.';
      } else if (val.length < 4) {
        return 'Please insert a description longer thatn 4 characters.';
      } else if (val.length > 7) {
        return 'Please insert a description shorter than 20 characters.';
      }
    },
    // handles inline style property if the text is not on error status
    stylesHandle() {
      const val = this.vue.textarea.trim();
      if (val != '' && val.length >= 4 && val.length < 7) {
        return 'color: green !important';
      }
      
    }
  },
  methods: {
    // prints message AND resets message input after displaying it
    // this is necessary in order to control the form validation
    validateForm() {
      alert(this.strBuild());
      this.vue.resultStr = '';
    },
    strBuild() {
      // iterate through the whole object valdata using only its keys
      for (const key in this.valData) {
        // get the value of the ovject through object[key] access model
        // if it is empty, fill in the validation string with its value and
        // communicating that it is empty to the user
        if (this.valData[key] == '') {
          this.vue.resultStr += `You need to fill in the ${key}.\n`;
        }
      }
        // if the string is empty that means no fields are left empty
        // thus sucess message is shown, else error message is shown (the one
        // built with the for loop)
        return this.vue.resultStr == ''? 'Form submitted sucessfully.':this.vue.resultStr ;  
    },
  },
});
  
  