/*
1. Afegirem al HTML un formulari amb 3 input text amb les seguents dades: nom, cognoms, mail. 

2. Afegirem al formulari un radiobutton pel tipus d'usuari: amb dos valors: client, venedor.

3. Afegirem al formulari dos checkboxs. Un amb interessos del client, i un altre amb 
interessos del venedor. 

4. Afegirem un event amb vue, al clickar al radio button, si està seleccionat client 
escriurem els interessos client, si està seleccionat venedor, mostrem interessos venedor. 
( aquesta informació sortirà a la part inferior del HTML, podeu fer servir una capa ). 

5. Crearem un botó enviar: Aquest botó cridarà una funció que enviarà el formulari 
( submit). Però de moment evitem enviar el formulari i en el seu defecte mostrem un missatge. 
*/
const vm = new Vue({
  el: "#myApp",
  data: {
    // generic data (not used for form validation) 
    vue: {
      // will toggle the vendor or client fields
      vendor: null,
      // used for form validation result display
      resultStr: '',
    },
    // data used for form validation (kept separate for better validation)
    valData: {
      name: '',
      surname: '',
      mail: '',
    },
  },
  methods: {
    // prints message AND resets message input after displaying it
    // this is necessary in order to control the form validation
    validateForm() {
      alert(this.strBuild());
      this.vue.resultStr = '';
    },
    strBuild() {
      // iterate through the whole object valdata using only its keys
      for (const key in this.valData) {
        // get the value of the ovject through object[key] access model
        // if it is empty, fill in the validation string with its value and
        // communicating that it is empty to the user
        if (this.valData[key] == '') {
          this.vue.resultStr += `You need to fill in the ${key}.\n`;
        }
      }
        // if the string is empty that means no fields are left empty
        // thus sucess message is shown, else error message is shown (the one
        // built with the for loop)
        return this.vue.resultStr == ''? 'Form submitted sucessfully.':this.vue.resultStr ;  
    },
  },
});
  
  