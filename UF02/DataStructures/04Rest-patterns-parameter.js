///////////////////////////////////////
// Rest Pattern and Parameters
// Rest parameter is an improved way to handle function parameter, allowing us to more easily handle various input as parameters in a function. 
// The rest parameter syntax allows us to represent an indefinite number of arguments as an array. 
// With the help of a rest parameter a function can be called with any number of arguments, no matter how it was defined. 
// Rest parameter is added in ES2015 or ES6 which improved the ability to handle parameter.

'use strict';

//const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],
  openingHours: {
    ["thu"]: {
      open: 12,
      close: 22,
    },
    ["fri"]: {
      open: 11,
      close: 23,
    },
    ["sat"]: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },

  order(starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  orderDelivery({ starterIndex = 1, mainIndex = 0, time = '20:00', address }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`
    );
  },

  orderPasta(ing1, ing2, ing3) {
    console.log(
      `Here is your declicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },

  orderPizza(mainIngredient, ...otherIngredients) {
    console.log(mainIngredient);
    console.log(otherIngredients);
  },
};

// 1) Destructuring
// SPREAD, because on RIGHT side of =
const arr = [1, 2, ...[3, 4]];
console.log(arr);

// REST, because on LEFT side of =
const [a, b, ...others] = [1, 2, 3, 4, 5];
console.log(a, b, others);

const [a1, a2, other2] = [1,2,3];
console.log(a1,a2,other2);

const [Pizza, , Risotto, ...otherfood] = [
  ...restaurant.mainMenu,
  ...restaurant.starterMenu,
];

// juntem en un array mainMenu + starterMenu
// al Pizza, li assignem la 1era posició del Newarray
// al Risotto, li assignem la 3era posició del Newarray
// a otherfood li assignem la resta de plats.
console.log("**** pizza, risoto, otherfood ***");
console.log(Pizza, Risotto, otherfood );

// exemple Objects
console.log("object weekday")
console.log(restaurant.openingHours)
const { sat, ...weekdays } = restaurant.openingHours ;
console.log(sat);
console.log(weekdays);

// exemple Functions
console.log("funciones")
const add = function (...numbers) {
  let sum = 0;
  for (let i = 0; i < numbers.length; i++) sum += numbers[i];
  console.log(sum);
};

add(2, 3);
add(5, 3, 7, 2);
add(8, 2, 5, 3, 2, 1, 4);

const x = [23, 5, 7];
add(...x);

restaurant.orderPizza('mushrooms', 'onion', 'olives', 'spinach');
restaurant.orderPizza('mushrooms');