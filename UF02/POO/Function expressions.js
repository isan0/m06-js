///////////////////////////////////////
// Object Methods

const jonas = {
    firstName: 'Jonas',
    lastName: 'Schmedtmann',  
    birthYeah: 1991,   
    job: 'teacher',   //string value
    friends: ['Michael', 'Peter', 'Steven'],  //array value
    hasDriversLicense: true,  // boolean value
  
    // Anem optimitzant 1er pas: 
    // calcAge: function (birthYeah) {
    //   return 2037 - birthYeah;
    // }
    // En aquest cas la crida seria jonas.calcAge(1991). 
    // Pero com el birthYeah està ja declarat al objecte, no l'hem de tornar a passar. Per aixo fem servir el this.
  
    //2on pas: Afegim el this per fer la crida a la variable birthYeah 
    // calcAge: function () {
    //   return 2037 - this.birthYeah;
    // }
    // Això es correcte, però encara ho podem millorar... 
    // es a dir guardant el resultat del calul a una variable del objecte. 
    // De manera que el valor el tindrem accedible desde qualsevol moment. 
  
    // Aquesta és Expression Function value
    // guardem el valor de la edad en una variable
    calcAge: function () {
      this.age = 2037 - this.birthYeah;
      return this.age;
    },
  
    // Fer una funció que escrigui un resum de totes les dades de Jonas:
    // Jonas is a 46 years old teacher, an he has a drivers's license.
    getSummary: function () {
      return `${this.firstName} is a ${this.calcAge()}-year old ${jonas.job}, and he has ${this.hasDriversLicense ? 'a' : 'no'} driver's license.`
    }
  };
  
  console.log(jonas.calcAge());
  console.log(jonas.age);
  console.log(jonas.age);
  console.log(jonas.age);